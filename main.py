from tkinter import *
from tkinter import font
from tkinter import messagebox
import os

root = Tk()


def window(main):
    root.title('Ingredient Helper')
    root.geometry('500x500')
    root.iconbitmap("icon.ico")


def getMat():
    DeleteAllBlanks()
    with open('recipes.txt', 'r+') as f:
        i = 1
        lines = f.readlines()
        global no_of_items
        no_of_items = len(lines)
        if len(lines)%3 != 0:
            messagebox.showerror("ERROR", "recipes.txt in wrong format. It should be:"
                                          "\nItemName:Ingredient1:Ingredient2..."
                                          "\nQuantitiy1:Quantity2..."
                                          "\nWeight1:Weight2..."
                                          "\n\nDelete recipes.txt and restart or fix file manually."
                                          "\nTo Add or edit files without risk click on <File> then click on the "
                                          "appropriate option.", icon='warning')
        global item_names
        item_names = []
        global item_quantities
        item_quantities = []
        global item_weights
        item_weights = []
        for line in lines:
            if i == 1:
                item_names.append(line.rstrip('\n').split(":"))
            elif i == 2:
                item_quantities.append(line.rstrip('\n').split(":"))
            elif i == 3:
                item_weights.append(line.rstrip('\n').split(":"))
            i += 1
            if i == 4:
                i = 1

        recipes = []
        for i in range(0, int(no_of_items / 3)):
            recipes.append(item_names[i][0])
        return recipes


def calculate(amount, have_item, recipe, quantities):
    # Finding index of the item you have
    index = recipe.index(have_item)
    global no_crafts
    if index == 0:
        no_crafts = amount
        timeToCraft(False, amount)
        weight(False, amount)
        box.config(state=NORMAL)
        if stickySV.get() == 0:
            box.delete(1.0, END)
        else:
            box.delete("here", END)
            box.insert("here", "\n\n")
        box.insert(INSERT, ("To make %d %s you need:\n" % (amount, have_item)))
        for i in range(0, len(quantities)):
            box.insert(INSERT, ("%d %s\n" % (int(quantities[i]) * amount, recipe[i + 1])))
        box.config(state=DISABLED)
    else:
        # Saving the required amount of the item you have for 1 craft

        req_amount = quantities[index - 1]

        new_quantities = []

        no_crafts = amount // int(req_amount)
        timeToCraft(False, no_crafts)
        weight(False, no_crafts)

        box.config(state=NORMAL)
        if stickySV.get() == 0:
            box.delete(1.0, END)
        else:
            box.delete("here", END)
            box.insert("here", "\n\n")
        box.insert(INSERT, ("%d %s: \nMakes %s %d times: \n" % (amount, have_item, recipe[0], no_crafts)))
        for i in range(0, len(quantities)):
            new_quantities.append(int(quantities[i]) * no_crafts)
            box.insert(INSERT, ("%d %s\n" % (new_quantities[i], recipe[i + 1])))
        box.config(state=DISABLED)


def initialRecipes():
    try:
        os.stat("recipes.txt")
    except:
        with open("recipes.txt", "w+") as file:
            file.write("Meat Stew:Meat:Flour:Cooking Wine:Mineral Water\n")
            file.write("5:2:2:3\n")
            file.write("0.03:0.10:0.01:0.01\n")
            file.write("Honey Wine:Cooking Honey:Essence of Liquor:Sugar:Mineral Water\n")
            file.write("3:2:2:6\n")
            file.write("0.10:0.01:0.01:0.01\n")
            file.write("Tea With Fine Scent:Flower:Fruit:Mineral Water:Cooking Honey\n")
            file.write("1:4:7:3\n")
            file.write("0.10:0.10:0.01:0.10\n")
            file.write("Boiled Bird Eggs:Eggs:Mineral Water:Cooking Wine:Salt\n")
            file.write("3:6:1:1\n")
            file.write("0.01:0.01:0.01:0.01\n")
            file.write("Fried Fish:Dried Fish:Flour:Deep Frying Oil\n")
            file.write("2:3:2\n")
            file.write("0.50:0.10:0.01\n")
            file.write("Beer:Cereal:Mineral Water:Sugar:Leavening Agent\n")
            file.write("5:6:1:2\n")
            file.write("0.10:0.01:0.01")


def textBox(First):
    with open('recipes.txt', 'r+') as f:
        i = 1
        lines = f.readlines()
        no_of_items = len(lines)
        item_names = []
        item_quantities = []
        global box
        selected_item = default.get()
        if First:
            boxScrollBar = Scrollbar(root)
            boxScrollBar.pack(side=RIGHT, fill=Y, pady=(105, 0))
            box = Text(root, relief=SOLID, height=21, width=26, wrap=WORD, font="7.5", yscrollcommand=boxScrollBar.set)
            boxScrollBar.config(command=box.yview)

        for line in lines:
            for line in lines:
                if i == 1:
                    item_names.append(line.rstrip('\n').split(":"))
                elif i == 2:
                    item_quantities.append(line.rstrip('\n').split(":"))
                elif i == 3:
                    item_weights.append(line.rstrip('\n').split(":"))
                i += 1
                if i == 4:
                    i = 1
        box.config(state=NORMAL)
        if stickySV.get() == 0:
            box.delete(1.0, END)
        else:
            box.configure(state=DISABLED)
        for i in range(0, int(no_of_items / 3)):
            if selected_item == item_names[i][0]:
                box.insert(INSERT, ("%s: \n" % item_names[i][0]))
                for j in range(0, len(item_quantities[i])):
                    box.insert(INSERT, ("-%s " % item_quantities[i][j]))
                    box.insert(INSERT, ("%s\n" % item_names[i][j + 1]))

        box.place(x=242, y=110)
        box.config(state=DISABLED)


def createOptions(Exists):
    DeleteAllBlanks()

    def callback(x, ItemName, recipe, quantity):
        set = [e.get() for e in quantity_entry]
        GreenBoxes = 0
        if stickySV.get() == 1:
            box.mark_set("delete", "here")
            box.mark_gravity("delete", LEFT)
        for i in range(0, int(len(set))):
            if quantity_entry[i].get() != "":
                quantity_entry[i].config(bg="Light Blue")
                amountItem = quantity_entry[i].get()
                z = i
                GreenBoxes += 1
            else:
                if i != 0:
                    quantity_entry[i].config(bg="Orange")
        if GreenBoxes == 1:
            calculate(int(amountItem), ItemName, recipe, quantity)
            quantity_entry[z].config(bg="Light Green")
        elif GreenBoxes > 1:
            rquantity = []
            for i in range(0,int(len(set))):
                if quantity_entry[i].get() == "" or i == 0:
                    rquantity.append(float('inf'))
                else:
                    rquantity.append(float(quantity_entry[i].get())//int(quantity[i-1]))
            index = rquantity.index(min(rquantity))
            if index != 0:
                quantity_entry[index].config(bg="Light Green")
            calculate(int(quantity_entry[index].get()),item_names[Index][index],recipe,quantity)
        elif GreenBoxes == 0:
            textBox(False)
            timeToCraft(False, 0)
            weight()
            global no_crafts
            no_crafts = 0
            for i in range(0, int(len(set))):
                quantity_entry[i].config(bg="white")
            if stickySV.get() == 1:
                box.configure(state=NORMAL)
                box.delete("delete", END)
                box.configure(state=DISABLED)

    if not Exists:
        global options_frame
    if Exists:
        options_frame.destroy()
    selected_item = default.get()
    options_frame = Frame(root)
    options_frame.place(x=0, y=25)
    # options_frame.pack()
    recipe_labels = []
    quantity_entry = []

    # label_frame = Frame(options_frame, height=450, width=165)
    # label_frame.pack(side=LEFT, fill=Y, padx=(20, 3), ipady=100)
    # label_frame.pack_propagate(False)

    #
    def onFrameConfigure(canvas):
        # Reset the scroll region to encompass the inner frame
        canvas.configure(scrollregion=canvas.bbox("all"))

    def _on_mousewheel(event):
        canvas.yview_scroll(-1 * int(event.delta / 120), "units")

    canvas = Canvas(options_frame, borderwidth=0, width=205, height=450)
    label_frame = Frame(canvas, width=165)
    label_frame.pack(side=LEFT, fill=Y, padx=(20, 3))
    vsb = Scrollbar(options_frame, orient="vertical", command=canvas.yview)
    canvas.configure(yscrollcommand=vsb.set)
    vsb.pack(side="left", fill=Y)
    canvas.pack(side="left", fill="both", expand=True)
    canvas.create_window((4, 4), window=label_frame, anchor="nw")

    label_frame.bind("<Configure>", lambda event, canvas=canvas: onFrameConfigure(canvas))
    Index = getMat().index(selected_item)
    global optionsEntrySV
    optionsEntrySV = []
    for i in range(0, int(len(item_names[Index]))):
        optionsEntrySV.append(StringVar())
        optionsEntrySV[i].trace("w",
                                lambda name, index, mode, var=optionsEntrySV[i], i=i: callback(i, item_names[Index][i],
                                                                                               item_names[Index],
                                                                                               item_quantities[Index]))
        recipe_labels.append(
            Label(label_frame, text=item_names[Index][i], relief=GROOVE, anchor="e", font="10", wraplength=125))
        quantity_entry.append(Entry(label_frame, textvariable=optionsEntrySV[i], bd=5, width=5))
        if i == 0:
            menu = OptionMenu(*(label_frame, default) + tuple(menu_options), command=textandoptions)
            menu.config(width=10, wraplength=100, padx=8, font=10)
            menu.grid(rowspan=2, columnspan=2, sticky=W, pady=5)
            quantity_entry[i].grid(row=i + 1, column=1, sticky=W, pady=5, padx=5)
        if i != 0:
            recipe_labels[i].grid(row=i + 2, column=0, sticky=E, pady=5)
            recipe_labels[i].config(width=16)
            quantity_entry[i].grid(row=i + 2, column=1, sticky=W, pady=5, padx=5)


def textandoptions(self):
    textBox(False)
    createOptions(True)


def character_limit(entry_text):
    if len(entry_text.get()) > 0:
        entry_text.set(entry_text.get()[:20])


# Initializing text file
initialRecipes()


def MakeEntryWindow(edit, selectedRecipe):
    def RemoveIngredient():
        size = (int(len(ingredientLabel))) - 1
        Ay = 80 + (55 * size) - 55
        if size > 0:
            EntrySV.pop(size)
            QuantitySV.pop(size)
            weightSV.pop(size)
            ingredientLabel[size].destroy()
            ingredientLabel.pop(size)
            ingredientEntry[size].destroy()
            ingredientEntry.pop(size)
            ingredientQuantity[size].destroy()
            ingredientQuantity.pop(size)
            weightEntry[size].destroy()
            weightEntry.pop(size)
            AddIngButton.place(x=305, y=Ay)
            SubIngButton.place(x=325, y=Ay)
            confirmButton.place(x=85, y=Ay + 40)

    def AddIngredient(looptimes=1):
        for _ in range(looptimes):
            size = (int(len(ingredientLabel))) - 1
            Ly = 62 + (55 * (size + 1))
            Ey = 89 + (55 * (size + 1))
            Ay = 80 + (55 * (size + 1))
            Fy = int(frame.winfo_height()) + 55
            EntrySV.append(StringVar())
            QuantitySV.append(StringVar())
            weightSV.append(StringVar())
            EntrySV[size + 1].trace("w", lambda *args: character_limit(EntrySV[size + 1]))
            weightEntry.append(Entry(frame, width=4, bd=3, textvariable=weightSV[size + 1], font="8"))
            ingredientEntry.append(Entry(frame, width=17, bd=3, textvariable=EntrySV[size + 1], font="8"))
            ingredientQuantity.append(Entry(frame, width=5, bd=3, textvariable=QuantitySV[size + 1], font="8"))
            ingredientLabel.append(Label(frame, text="*Ingredient %d" % (size + 2), font="8", relief=SUNKEN))
            ingredientLabel[size + 1].place(x=85, y=Ly)
            ingredientEntry[size + 1].place(x=85, y=Ey)
            ingredientQuantity[size + 1].place(x=250, y=Ey)
            weightEntry[size + 1].place(x=25, y=Ey)
            AddIngButton.place(x=305, y=Ay)
            AddIngButton.configure(takefocus=0)
            SubIngButton.place(x=325, y=Ay)
            SubIngButton.configure(takefocus=0)
            confirmButton.place(x=85, y=Ay + 40)
            if size > 3:
                frame.configure(height=Fy)

    EntryWindow = Toplevel(root)
    EntryWindow.title('Add Recipe')
    EntryWindow.geometry('370x400')
    EntryWindow.iconbitmap("icon.ico")
    EntryWindow.resizable(0, 0)
    EntryWindow.focus_force()

    #
    def onFrameConfigure(canvas):
        # Reset the scroll region to encompass the inner frame
        canvas.configure(scrollregion=canvas.bbox("all"))

    def _on_mousewheel(event):
        canvas.yview_scroll(-1 * int(event.delta / 120), "units")

    canvas = Canvas(EntryWindow, borderwidth=0)
    frame = Frame(canvas, width=500, height=400)
    vsb = Scrollbar(EntryWindow, orient="vertical", command=canvas.yview)
    canvas.configure(yscrollcommand=vsb.set)
    vsb.pack(side="right", fill="y")
    canvas.pack(side="left", fill="both", expand=True)
    canvas.create_window((4, 4), window=frame, anchor="nw")

    frame.bind("<Configure>", lambda event, canvas=canvas: onFrameConfigure(canvas))
    canvas.bind_all("<MouseWheel>", _on_mousewheel)

    #
    def Add():
        size = int(len(ingredientEntry))
        proceed = True
        recipeName = recipeEntry.get()
        if recipeEntry.get() == "":
            recipeEntry.config(bg="orange")
            proceed = False
        for i in range(size):
            if ingredientEntry[i].get() == "":
                ingredientEntry[i].config(bg="orange")
                proceed = False
            if ingredientQuantity[i].get() == "":
                ingredientQuantity[i].config(bg="orange")
                proceed = False
        if proceed:
            recipeEntry.config(bg="light green")
            for i in range(size):
                if ingredientEntry[i].get() != "":
                    ingredientEntry[i].config(bg="light green")
                if ingredientQuantity[i].get() != "":
                    ingredientQuantity[i].config(bg="light green")
                if weightEntry[i].get() != "":
                    weightEntry[i].config(bg="light green")
            if edit:
                result = messagebox.askquestion("Confirm", 'Edit "%s"?' % default.get(),
                                                icon="question")
            else:
                result = messagebox.askquestion("Confirm", 'Add "%s" to recipe list?' % recipeEntry.get(),
                                                icon="question")
            if result == 'yes':
                if edit:
                    DeleteRecipe(False)
                with open("recipes.txt", "a") as f:
                    rEntrywithoutcolon = recipeEntry.get().replace(":", "")
                    f.write("\n%s" % rEntrywithoutcolon)
                    for i in range(0, size):
                        iEntrywithoutcolon = ingredientEntry[i].get().replace(":", "")
                        f.write(":%s" % iEntrywithoutcolon)  # ingredientEntry[i]
                    for i in range(0, size):
                        qEntrywithoutcolon = ingredientQuantity[i].get().replace(":", "")
                        if i == 0:
                            f.write("\n%s" % qEntrywithoutcolon)  # ingredientQuantity[i]
                        else:
                            f.write(":%s" % qEntrywithoutcolon)
                    for i in range(0,size):
                        if weightEntry[i].get() == "":
                            wEntrywithoutcolon = 0
                        else:
                            wEntrywithoutcolon = weightEntry[i].get().replace(":","")
                        if i== 0:
                            f.write("\n%s" % wEntrywithoutcolon)
                        else:
                            f.write(":%s" % wEntrywithoutcolon)
                EntryWindow.destroy()
                DeleteAllBlanks()
                global menu_options
                menu_options = getMat()
                index = menu_options.index(recipeName)
                default.set(menu_options[index])
                createOptions(False)
                textBox(False)
                root.focus_force()

    def Cancel():
        EntryWindow.destroy()
        root.focus_force()

    confirmButton = Button(frame, text="Add Recipe", command=Add)
    confirmButton.place(x=85, y=120)
    CancelButton = Button(canvas, text="Cancel", command=Cancel, takefocus=0)
    CancelButton.pack(anchor=SE)
    sv = StringVar()
    sv.trace("w", lambda *args: character_limit(sv))
    recipeLabel = Label(frame, text="*Recipe Name", font="10", relief=RAISED)
    recipeLabel.place(x=85, y=8)
    recipeEntry = Entry(frame, width=17, bd=3, textvariable=sv, font="8")
    recipeEntry.place(x=85, y=30)
    recipeEntry.focus_force()
    helv36 = font.Font(family="Helvetica", size=13, weight="bold")
    AddIngButton = Button(frame, text="+", command=AddIngredient, width=1)
    SubIngButton = Button(frame, text="-", command=RemoveIngredient, width=1)
    AddIngButton['font'] = helv36
    SubIngButton['font'] = helv36
    AddIngButton.place(x=305, y=80)
    SubIngButton.place(x=325, y=80)
    ingredientLabel = []
    ingredientEntry = []
    ingredientQuantity = []
    weightEntry = []
    weightSV = [StringVar()]
    EntrySV = [StringVar()]
    QuantitySV = [StringVar()]
    EntrySV[0].trace("w", lambda *args: character_limit(EntrySV[0]))
    weightLabel = Label(frame, text="LT:", font="8", relief=GROOVE)
    weightLabel.place(x=25,y=62)
    if not edit:
        weightEntry.append(Entry(frame, width=4, bd=3, textvariable=weightSV[0], font="8"))
        ingredientEntry.append(Entry(frame, width=17, bd=3, textvariable=EntrySV[0], font="8"))
        ingredientQuantity.append(Entry(frame, width=5, bd=3, textvariable=QuantitySV[0], font="8"))
        ingredientLabel.append(Label(frame, text="*Ingredient 1", font="8", relief=SUNKEN))
        QuantityLabel = Label(frame, text="*Amount:", font="8", relief=GROOVE)
        ingredientLabel[0].place(x=85, y=62)
        QuantityLabel.place(x=250, y=62)
        ingredientEntry[0].place(x=85, y=89)
        ingredientQuantity[0].place(x=250, y=89)
        weightEntry[0].place(x=25, y=89)
        AddIngredient(3)

    if edit:
        global item_names
        global item_quantities
        global item_weights
        RecipeIndex = getMat().index(selectedRecipe)
        size = int(len(item_quantities[RecipeIndex]))
        sv.set(item_names[RecipeIndex][0])
        for i in range(size):
            EntrySV[i].set(item_names[RecipeIndex][i + 1])
            QuantitySV[i].set(item_quantities[RecipeIndex][i])
            weightSV[i].set(item_weights[RecipeIndex][i])
            AddIngredient()
            EntryWindow.update()


def DeleteRecipe(warning):
    selectedRecipe = default.get()
    global menu_options
    if len(menu_options) > 1:
        if warning:
            result = messagebox.askquestion("", "Delete <%s>" % selectedRecipe, icon='warning')
        elif not warning:
            result = 'yes'
        if result == 'yes':
            with open("recipes.txt", "r+") as f:
                lines = f.readlines()
                size = int(len(lines))
                f.seek(0)
                skip = False
                skip2 = False
                for i in range(0, size):
                    if skip or skip2:
                        if skip:
                            skip2 = True
                            skip = False
                            continue
                        if skip2:
                            skip2 = False
                            continue

                    if selectedRecipe in lines[i]:
                        skip = True
                        continue
                    if i == size:
                        f.write(lines[i].strip('\n'))
                        continue
                    f.write(lines[i])
                f.truncate()
    else:
        messagebox.showwarning("", "Only 1 recipe remaining", icon='error')
    menu_options = getMat()
    default.set(menu_options[0])
    createOptions(False)


def DeleteAllBlanks():
    with open("recipes.txt", "r") as fh:
        lines = fh.readlines()
    lines = filter(lambda x: not x.isspace(), lines)
    with open("recipes.txt", "w") as fh:
        fh.write("".join(lines))


def clearBox():
    stickySV.set(0)
    box.configure(state=NORMAL)
    box.delete(1.0, END)
    box.configure(state=DISABLED)
    global optionsEntrySV
    for i in range(int(len(optionsEntrySV))):
        optionsEntrySV[i].set("")


def sticky():
    box.configure(state=NORMAL)
    box.insert(INSERT, "<<")
    box.configure(state=DISABLED)
    box.mark_set("here", box.index(INSERT))
    box.mark_gravity("here", LEFT)
    stickySV.set(1)


def timeToCraft(First=True, amount=0):
    global no_crafts
    if First:
        textSV.set("00:00:00")
        for i in range(3):
            timeSV.append(StringVar())
            timeSV[i].trace("w", lambda name, index, mode, var=timeSV[i]: timeToCraft(False, no_crafts))
            timeEntry.append(Entry(root, width=5, textvariable=timeSV[i]))
        hlabel = Label(root, text="h", relief=FLAT)
        mlabel = Label(root, text="m", relief=FLAT)
        slabel = Label(root, text="s", relief=FLAT)
        label = Label(root, text="Time to craft all:", relief=FLAT)
        tlabel = Label(root, textvariable=textSV, relief=GROOVE, width=13, height=1, justify=CENTER)
        timeEntry[0].place(x=252, y=55)
        hlabel.place(x=280, y=55)
        label.place(x=398, y=38)
        timeEntry[1].place(x=300, y=55)
        mlabel.place(x=328, y=55)
        timeEntry[2].place(x=348, y=55)
        slabel.place(x=376, y=55)
        tlabel.place(x=400, y=55)

    total = 0
    time = [0] * 3
    for i in range(3):
        if timeSV[i].get() != "":
            if i == 0:
                total += float(timeSV[i].get().strip()) * 3599
            if i == 1:
                total += float(timeSV[i].get().strip()) * 60
            else:
                total += float(timeSV[i].get().strip())
    if total > 0:
        ntotal = total * amount
        time[1], time[2] = divmod(ntotal, 60)
        time[0], time[1] = divmod(time[1], 60)
        textSV.set("%02d:%02d:%02d" % (time[0], time[1], time[2]))
    else:
        textSV.set("00:00:00")


def weight(First=True, crafts=0):
    global item_weights
    global item_quantities
    names = getMat()
    name = default.get()
    index = names.index(name)
    weightSV = StringVar()
    if First:
        weightSV.set("0.00 lt")
    if not First:
        totalWeight = 0
        for i in range(0, int(len(item_weights[index]))):
            totalWeight += (float(item_weights[index][i])) * int(item_quantities[index][i])
        totalWeight *= crafts
        weightSV.set("%.2f lt" % totalWeight)
    wLabel = Label(root, textvariable=weightSV, relief=GROOVE, width=13, height=1, justify=CENTER)
    wLabel.place(x=400, y=15)


def AboutWindow():
    messagebox.showinfo("Help", "+Click on <File> to add, edit or remove recipes."
                                "\n     -Adding LT to a recipe is optional, rest isn't."
                                "\n\n+Click on <Stick Recipe> to pin the current text in the box."
                                "\n     -Click again to pin any new text added."
                                "\n     -Anything above '<<' is pinned."
                                "\n+Click on <Clear> to clear the pinned text and the entry boxes."
                                "\n\n+LT shown is how much LT the amount of materials shown in the text box will occupy."
                                "\n\n+Avoid modifying .txt file manually."
                                "\n     -If any problem occurs try closing the program, "
                                "deleting 'recipes.txt' and reopening. "
                                "\n       Warning: this will delete all manually added recipes."
                                "\n\nAny questions or problems PM Xain#6873 on discord", icon='info')


no_crafts = 0
DeleteAllBlanks()
# Menu
default = StringVar(root)
stickySV = IntVar()
menu_options = getMat()
default.set(menu_options[0])
textBox(True)
createOptions(False)
menuBar = Menu(root)
fileMenu = Menu(menuBar, tearoff=0)
fileMenu.add_command(label="Add new recipe", command=lambda: MakeEntryWindow(False, default.get()))
fileMenu.add_command(label="Edit selected recipe", command=lambda: MakeEntryWindow(True, default.get()))
fileMenu.add_command(label="Delete selected recipe", command=lambda: DeleteRecipe(True))
fileMenu.add_separator()
fileMenu.add_command(label="Exit", command=root.quit)
menuBar.add_cascade(label="File", menu=fileMenu)
menuBar.add_command(command=AboutWindow, label="Instructions")
root.config(menu=menuBar)

stickyRecipeButton = Checkbutton(root, text="Stick recipe", variable=stickySV, indicatoron=0,
                                 selectcolor="light green", command=sticky, takefocus=0)
stickyRecipeButton.place(x=242, y=84)
clearBoxButton = Button(root, text="Clear", command=clearBox, takefocus=0)
clearBoxButton.place(x=437, y=83)  # 72
textSV = StringVar()
timeSV = []
timeEntry = []
timeToCraft()
weight()
# END
window(root)
mainloop()
